/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    // Relative to /src/views
    path: '/home',
    name: 'Home',
    view: 'Home',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/user-profile',
    name: 'Profile',
    view: 'Profile',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/administration',
    name: 'Management',
    view: 'Administration',
    meta: {
      requireAuth: true,
      isAdmin: true
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    view: 'Settings',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/create',
    name: 'Create',
    view: 'Create',
    meta: {
      requireAuth: true,
      isAdmin: true
    }
  },
  {
    path: '/subject/:id',
    name: 'SubjectList',
    view: 'CourseList',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/subject',
    name: 'Subject',
    view: 'SelectCourse',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/transcript',
    name: 'Transcript',
    view: 'StudentTranscript',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/notifications',
    name: 'Notifications',
    view: 'Notifications'
  },
  {
    path: '/login',
    name: 'Login',
    view: 'Login'
  }
]
