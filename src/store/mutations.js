// https://vuex.vuejs.org/en/mutations.html

export default {
  setAuthentication (state, payload) {
    state.isSignedIn = payload
  }
}
