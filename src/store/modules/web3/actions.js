import Web3 from 'web3'
// import { genPath } from 'vuetify/lib/components/VSparkline/helpers/path'

export default {
  connectToWeb3: ({
    commit
  }) => {
    return new Promise((resolve, reject) => {
      const provider = new Web3.providers.HttpProvider('https://regsys.ml/eth')
      if (provider !== null) {
        const web3 = new Web3(provider)
        let admin
        web3.eth.getAccounts().then(accounts => {
          admin = accounts[0]
        })
          .then(() => {
            commit('setAdmin', admin)
          })
        web3.eth.net.isListening()
          .then(() => {
            commit('setInjected', true)
            commit('setInstance', () => web3)
            console.log('web3 connected')
            resolve(web3)
          })
          .catch(e => commit('setInjected', false))
      } else {
        reject(new Error('Unable to connect to Ethereum Server'))
      }
    })
  },
  setContract: ({
    commit,
    state
  }) => {
    const accountABI = [
      {
        'constant': true,
        'inputs': [],
        'name': 'getAddress',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'addr',
            'type': 'address'
          },
          {
            'name': 'email',
            'type': 'bytes32'
          }
        ],
        'name': 'addUser',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'owner',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [
          {
            'name': '',
            'type': 'bytes32'
          }
        ],
        'name': 'accounts',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'inputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'constructor'
      }
    ]

    const teachABI = [
      {
        'constant': true,
        'inputs': [],
        'name': 'getAddress',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': '_email',
            'type': 'bytes32'
          }
        ],
        'name': 'authorize',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'name',
            'type': 'bytes32'
          }
        ],
        'name': 'addTeacher',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'name': 'teachers',
        'outputs': [
          {
            'name': '',
            'type': 'bytes32'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'inputs': [
          {
            'name': '_addr',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'constructor'
      }
    ]

    const corsABI = [
      {
        'constant': true,
        'inputs': [
          {
            'name': 'code',
            'type': 'bytes8'
          }
        ],
        'name': 'getLecturer',
        'outputs': [
          {
            'name': '',
            'type': 'bytes32'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'getAddress',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': '_email',
            'type': 'bytes32'
          }
        ],
        'name': 'authorize',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'name',
            'type': 'bytes32'
          },
          {
            'name': 'lecturer',
            'type': 'bytes32'
          },
          {
            'name': 'code',
            'type': 'bytes8'
          },
          {
            'name': 'credit',
            'type': 'uint256'
          },
          {
            'name': 'academicYear',
            'type': 'bytes8'
          }
        ],
        'name': 'addCourse',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'getNumberOfCourses',
        'outputs': [
          {
            'name': '',
            'type': 'uint256'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'stdCode',
            'type': 'bytes8'
          },
          {
            'name': 'corsCode',
            'type': 'bytes8'
          }
        ],
        'name': 'enroll',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [
          {
            'name': 'code',
            'type': 'bytes8'
          }
        ],
        'name': 'getCourse',
        'outputs': [
          {
            'name': '',
            'type': 'bytes32'
          },
          {
            'name': '',
            'type': 'bytes32'
          },
          {
            'name': '',
            'type': 'bytes8'
          },
          {
            'name': '',
            'type': 'uint256'
          },
          {
            'name': '',
            'type': 'bytes8'
          },
          {
            'name': '',
            'type': 'bytes8[]'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'getCourseList',
        'outputs': [
          {
            'name': '',
            'type': 'bytes8[]'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'inputs': [
          {
            'name': 'accManAddr',
            'type': 'address'
          },
          {
            'name': 'tAddr',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'constructor'
      }
    ]

    const stdABI = [
      {
        'constant': true,
        'inputs': [],
        'name': 'getStdList',
        'outputs': [
          {
            'name': '',
            'type': 'bytes8[]'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'getAddress',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': '_email',
            'type': 'bytes32'
          }
        ],
        'name': 'authorize',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [
          {
            'name': '',
            'type': 'bytes32'
          }
        ],
        'name': '_students',
        'outputs': [
          {
            'name': '',
            'type': 'bytes8'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [
          {
            'name': 'stdCode',
            'type': 'bytes8'
          },
          {
            'name': 'corsCode',
            'type': 'bytes8'
          }
        ],
        'name': 'getGrade',
        'outputs': [
          {
            'name': '',
            'type': 'uint256'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [
          {
            'name': '_code',
            'type': 'bytes8'
          }
        ],
        'name': 'getStudent',
        'outputs': [
          {
            'name': '',
            'type': 'bytes32'
          },
          {
            'name': '',
            'type': 'bytes32'
          },
          {
            'name': '',
            'type': 'bytes8'
          },
          {
            'name': '',
            'type': 'uint256'
          },
          {
            'name': '',
            'type': 'uint256'
          },
          {
            'name': '',
            'type': 'bytes32[]'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'getNumberOfStudents',
        'outputs': [
          {
            'name': '',
            'type': 'uint256'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'stdCode',
            'type': 'bytes8'
          },
          {
            'name': 'corsCode',
            'type': 'bytes8'
          },
          {
            'name': 'grade',
            'type': 'uint256'
          },
          {
            'name': 'credit',
            'type': 'uint256'
          }
        ],
        'name': 'enroll',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'fname',
            'type': 'bytes32'
          },
          {
            'name': 'lname',
            'type': 'bytes32'
          },
          {
            'name': '_code',
            'type': 'bytes8'
          }
        ],
        'name': 'addStudent',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'inputs': [
          {
            'name': 'accManAddr',
            'type': 'address'
          },
          {
            'name': 'tAddr',
            'type': 'address'
          },
          {
            'name': 'corsAddr',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'constructor'
      }
    ]

    const enrollABI = [
      {
        'constant': true,
        'inputs': [],
        'name': 'getAddress',
        'outputs': [
          {
            'name': '',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
      },
      {
        'constant': false,
        'inputs': [
          {
            'name': 'stdCode',
            'type': 'bytes8'
          },
          {
            'name': 'corsCode',
            'type': 'bytes8'
          },
          {
            'name': '_grade',
            'type': 'uint256'
          },
          {
            'name': 'credit',
            'type': 'uint256'
          }
        ],
        'name': 'enroll',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
      },
      {
        'inputs': [
          {
            'name': 'stdAddr',
            'type': 'address'
          },
          {
            'name': 'corsAddr',
            'type': 'address'
          }
        ],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'constructor'
      }
    ]

    const web3 = state.web3.instance()
    const accountAddr = web3.utils.toChecksumAddress('0x3bf1a1e8ae9ecf403d47015dee8ce12b675bc9ab')
    const teachAddr = web3.utils.toChecksumAddress('0x974f550b395235b543a6a4d204ebcb7509540cac')
    const corsAddr = web3.utils.toChecksumAddress('0xb28dc7914e666c6f6ee1f3ec34416fb3ba3ce404')
    const stdAddr = web3.utils.toChecksumAddress('0x9da12e35dda646d888764901a05b13a186c6bb0c')
    const enrollAddr = web3.utils.toChecksumAddress('0x4be853da93acd0ca7d55f3a81c6cac2d828a948d')

    const accountInstance = new web3.eth.Contract(accountABI, accountAddr)
    const teachInstance = new web3.eth.Contract(teachABI, teachAddr)
    const courseInstance = new web3.eth.Contract(corsABI, corsAddr)
    const studentInstance = new web3.eth.Contract(stdABI, stdAddr)
    const enrollmentInstance = new web3.eth.Contract(enrollABI, enrollAddr)
    commit('setContract', {
      'accountManager': accountInstance,
      'teacher': teachInstance,
      'course': courseInstance,
      'student': studentInstance,
      'enrollment': enrollmentInstance })
    console.log('contracts loaded')

    // resolve({ studentInstance, courseInstance, enrollmentInstance })
  },
  walletLoad: ({
    commit,
    state
  }, payload) => { // payload=pin
    return new Promise(async (resolve, reject) => {
      const studentInstance = state.web3.contract.student
      const email = localStorage.getItem('email')
      const walletName = 'wallet_' + email.split('@')[0]
      const web3 = state.web3.instance()

      try {
        const wallet = web3.eth.accounts.wallet.load(payload, walletName)
        localStorage.setItem('address', wallet[0]['address'])
        commit('setAddress', wallet[0]['address'])
        // authorize student
        await studentInstance.methods.authorize(web3.utils.stringToHex(localStorage.getItem('email')))
          .send({
            from: web3.utils.toChecksumAddress(localStorage.getItem('address')),
            gas: 8000000
          })
        // console.log('student authorized')

        resolve(wallet)
      } catch (e) {
        console.log('walletLoad error', e)
        reject(e)
      }
    })
  },
  pushStudent: ({
    commit,
    state
  }, payload) => { // payload = { name, code }
    return new Promise(async (resolve, reject) => {
      const web3 = state.web3.instance()
      const studentInstance = state.web3.contract.student
      try {
        // authorize student
        await studentInstance.methods.authorize(web3.utils.stringToHex(localStorage.getItem('email')))
          .send({
            from: web3.utils.toChecksumAddress(state.address),
            gas: 8000000
          })
        // console.log('student authorized')

        // get student
        let studentData = await studentInstance.methods.getStudent(payload.code)
          .call({
            from: web3.utils.toChecksumAddress(state.address),
            gas: 8000000
          })
        // console.log('student data', studentData)

        // add student if student not exists
        const studentCode = web3.utils.hexToString(studentData[1])
        // console.log('student code', studentCode)
        if (!studentCode || studentCode === '') {
          console.log('student not exist.')

          await studentInstance.methods.addStudent(payload.firstName, payload.lastName, payload.code)
            .send({
              from: web3.utils.toChecksumAddress(state.address),
              gas: 8000000
            })

          // get student
          studentData = await studentInstance.methods.getStudent(payload.code)
            .call({
              from: web3.utils.toChecksumAddress(state.address),
              gas: 8000000
            })
        } else {
          console.log('student exist.')
        }
        // console.log('student data', studentData)
        resolve(studentData)
      } catch (e) {
        console.log('pushStudent error', e)
        reject(e)
      }
    })
  },
  pushCourse: ({
    commit,
    state
  }, payload) => {
    return new Promise(async (resolve, reject) => {
      const web3 = state.web3.instance()
      const courseInstance = state.web3.contract.course
      try {
        // authorize course
        await courseInstance.methods.authorize(web3.utils.stringToHex(localStorage.getItem('email')))
          .send({
            from: web3.utils.toChecksumAddress(state.address),
            gas: 8000000
          })
        console.log('course authorized')

        // get course
        let courseData = await courseInstance.methods.getCourse(payload.code)
          .call({
            from: web3.utils.toChecksumAddress(state.address),
            gas: 8000000
          })
        // console.log('course data', courseData)

        // add course if course not exists
        const courseCode = web3.utils.hexToString(courseData[2])
        // console.log('course code', courseCode)
        if (!courseCode || courseCode === '') {
          console.log('course not exist.')

          // add course
          await courseInstance.methods.addCourse(
            payload.name,
            payload.lecturer,
            payload.code,
            payload.credit,
            payload.academic_year
          ).send({
            from: web3.utils.toChecksumAddress(state.address),
            gas: 8000000
          })

          // get course
          courseData = await courseInstance.methods.getCourse(payload.code)
            .call({
              from: web3.utils.toChecksumAddress(state.address),
              gas: 8000000
            })
          // console.log('new course data', courseData)
        } else {
          console.log('course exist.')
        }

        resolve(courseData)
      } catch (e) {
        console.log('pushCourse error', e)
        reject(e)
      }
    })
  },
  fetchCourse: ({
    commit,
    state
  }) => {
    return new Promise((resolve, reject) => {
      const web3 = state.web3.instance()
      const courseInstance = state.web3.contract.course
      commit('clearCourse')
      courseInstance.methods.getCourseList().call((err, result) => {
        if (err) {
          console.log(err)
          reject(new Error('Unable to fetch course'))
        }
        const lastIndex = result.length - 1
        let lecturer = ''
        result.forEach((obj, index) => {
          courseInstance.methods.getLecturer(obj.toString()).call((err, result) => {
            if (err) {
              console.log(err)
              reject(new Error('Unable to fetch course'))
            }
            lecturer = web3.utils.hexToString(result)
          }).then(() => {
            if (localStorage.getItem('lecturer') === lecturer) {
              courseInstance.methods.getCourse(obj.toString())
                .call((err, result) => {
                  if (err) {
                    console.log(err)
                    reject(new Error('Unable to fetch course'))
                  }
                  commit('appendCourse', {
                    'name': web3.utils.hexToString(result[0]),
                    'code': web3.utils.hexToString(result[2]),
                    'academic_year': web3.utils.hexToString(String(result[4]))
                  })
                  if (index === lastIndex) {
                    resolve()
                  }
                })
            }
          })
        })
      })
    })
  },
  fetchEnrolledStudent: ({ // get students in a course. payload is course code
    commit,
    state
  }, payload) => {
    return new Promise(async (resolve, reject) => {
      const courseInstance = state.web3.contract.course
      const studentInstance = state.web3.contract.student
      const web3 = state.web3.instance()

      // authorize student
      // await studentInstance.methods.authorize(web3.utils.stringToHex(localStorage.getItem('email')))
      //   .send({
      //     from: web3.utils.toChecksumAddress(localStorage.getItem('address')),
      //     gas: 8000000
      //   })

      // console.log('student authorized')

      courseInstance.methods.getCourse(web3.utils.stringToHex(payload))
        .call((err, result) => {
          if (err) {
            console.log(err)
            reject(new Error('Unable to fetch enroll student'))
          }
          commit('clearCourse')
          commit('setCourse', {
            code: web3.utils.hexToString(result[2]),
            name: web3.utils.hexToString(result[0])
          })
          commit('clearStudent')
          const lastIndex = result.length - 1
          result[5].forEach((obj, index) => {
            studentInstance.methods.getGrade(obj, web3.utils.stringToHex(payload)).call({
              // from: web3.utils.toChecksumAddress(localStorage.getItem('address'))
            }).then((result) => { // student code, course code
              // if (err) {
              //   console.log(err)
              //   reject(new Error('Unable to fetch enrolled student'))
              // }
              let grade = ''
              if (result / 10000 === 4) {
                grade = 'A'
              } else if (result / 10000 === 3.5) {
                grade = 'B+'
              } else if (result / 10000 === 3) {
                grade = 'B'
              } else if (result / 10000 === 2.5) {
                grade = 'C+'
              } else if (result / 10000 === 2) {
                grade = 'C'
              } else if (result / 10000 === 1.5) {
                grade = 'D+'
              } else if (result / 10000 === 1) {
                grade = 'D'
              } else if (result / 10000 === 0) {
                grade = 'F'
              }
              studentInstance.methods.getStudent(obj).call((err, result) => {
                if (err) {
                  console.log(err)
                  reject(new Error('Unable to fetch enroll student'))
                }
                commit('appendStudent', {
                  'firstName': web3.utils.hexToString(result[0]),
                  'lastName': web3.utils.hexToString(result[1]),
                  'code': web3.utils.hexToString(result[2]),
                  'grade': grade
                })
                if (index === lastIndex) {
                  resolve()
                }
              })
            })
          })
        })
    })
  },
  enrollment: ({ // map student with grade
    commit,
    state
  }, payload) => {
    return new Promise(async (resolve, reject) => {
      const web3 = state.web3.instance()
      const enrollmentInstance = state.web3.contract.enrollment
      try {
        // add enrollment
        const result = await enrollmentInstance.methods.enroll(
          payload.stdCode,
          payload.courseCode,
          payload.grade,
          payload.credit
        ).send({
          from: web3.utils.toChecksumAddress(state.address),
          gas: 8000000
        })

        resolve(result)
      } catch (e) {
        console.log('enrollment error', e)
        reject(e)
      }
    })
  },
  fetchStudentCourse: ({ // show transcript. payload is student code
    commit,
    state,
    dispatch
  }, payload) => {
    return new Promise(async (resolve, reject) => {
      const studentInstance = state.web3.contract.student
      const courseInstance = state.web3.contract.course
      const web3 = state.web3.instance()

      // const email = localStorage.getItem('email')
      // const walletName = 'wallet_' + email.split('@')[0]
      // const wallet = web3.eth.accounts.wallet.load('111111', walletName)

      // const address = wallet[0]['address']
      // const contractAddr = studentInstance.options.address
      // const transfer = studentInstance.methods.authorize(web3.utils.stringToHex(localStorage.getItem('email')))
      // const encodedABI = transfer.encodeABI()
      // const tx = {
      //   from: web3.utils.toChecksumAddress(address),
      //   to: contractAddr,
      //   gas: 8000000,
      //   data: encodedABI
      // }
      // web3.eth.accounts.signTransaction(tx, localStorage.getItem('privateKey'))
      //   .then(signedTx => {
      //     web3.eth.sendSignedTransaction(signedTx.rawTransaction)
      //       .on('receipt', receipt => {
      //         console.log(receipt)
      //         console.log('student authorized')
      //       })
      //   })

      // studentInstance.methods.getStudent(web3.utils.stringToHex(payload)).call((err, result) => {
      //   console.log('get student')
      //   if (err) {
      //     console.log(err)
      //     reject(new Error('Unable to fetch student course'))
      //   }
      //   console.log(result)
      //   commit('clearStudent')
      //   commit('clearCourse')
      //   commit('setStudent', {
      //     'firstName': web3.utils.hexToString(result[0]),
      //     'lastName': web3.utils.hexToString(result[1]),
      //     'code': web3.utils.hexToString(result[2]),
      //     'gpax': (result[4] / 10000.0).toFixed(2)
      //   })
      //   console.log(web3.utils.hexToString(result[0]))
      //   let grade = ''
      //   if (result / 10000 === 4) {
      //     grade = 'A'
      //   } else if (result / 10000 === 3.5) {
      //     grade = 'B+'
      //   } else if (result / 10000 === 3) {
      //     grade = 'B'
      //   } else if (result / 10000 === 2.5) {
      //     grade = 'C+'
      //   } else if (result / 10000 === 2) {
      //     grade = 'C'
      //   } else if (result / 10000 === 1.5) {
      //     grade = 'D+'
      //   } else if (result / 10000 === 1) {
      //     grade = 'D'
      //   } else if (result / 10000 === 0) {
      //     grade = 'F'
      //   }
      //   console.log('getting grade', grade)
      // })
      // try {
      //   console.log('state', state.web3)
      //   console.log('studentInstance', studentInstance)
      //   const res = await studentInstance.methods.getStudent('0x3538303131313031').call()
      //   console.log('result', res)
      //   resolve(res)
      // } catch (e) {
      //   console.log('error', e)
      //   reject(e)
      // }

      studentInstance.methods.getStudent(web3.utils.stringToHex(payload)).call((err, result) => {
        if (err) {
          console.log(err)
          reject(new Error('Unable to fetch student course'))
        }
        commit('clearStudent')
        commit('clearCourse')
        commit('setStudent', {
          'firstName': web3.utils.hexToString(result[0]),
          'lastName': web3.utils.hexToString(result[1]),
          'code': web3.utils.hexToString(result[2]),
          'gpax': (result[4] / 10000.0).toFixed(2)
        })
        const lastIndex = result.length - 1
        result[5].forEach((obj, index) => { // result[4] = array of course code
          studentInstance.methods.getGrade(web3.utils.stringToHex(payload), obj).call((err, result) => {
            if (err) {
              console.log(err)
              reject(new Error('Unable to fetch student course'))
            }
            let grade = ''
            if (result / 10000 === 4) {
              grade = 'A'
            } else if (result / 10000 === 3.5) {
              grade = 'B+'
            } else if (result / 10000 === 3) {
              grade = 'B'
            } else if (result / 10000 === 2.5) {
              grade = 'C+'
            } else if (result / 10000 === 2) {
              grade = 'C'
            } else if (result / 10000 === 1.5) {
              grade = 'D+'
            } else if (result / 10000 === 1) {
              grade = 'D'
            } else if (result / 10000 === 0) {
              grade = 'F'
            }
            courseInstance.methods.getCourse(obj).call((err, result) => {
              if (err) {
                console.log(err)
                reject(new Error('Unable to fetch student course'))
              }
              commit('appendCourse', {
                'name': web3.utils.hexToString(result[0]),
                'code': web3.utils.hexToString(result[2]),
                'credit': String(result[3]),
                'grade': grade,
                'academic_year': web3.utils.hexToString(String(result[4]))
              })
              if (index === lastIndex) {
                resolve()
              }
            })
          })
        })
      })
    })
  },
  addTeacherAccount: ({
    commit,
    state
  }, payload) => { // payload = email, name, pin
    return new Promise(async (resolve, reject) => {
      const web3 = state.web3.instance()
      const accountInstance = state.web3.contract.accountManager
      const teacherInstance = state.web3.contract.teacher
      const walletName = 'wallet_' + payload.email.split('@')[0]
      const wallet = web3.eth.accounts.wallet.create(1)
      localStorage.setItem('address', wallet[0]['address'])
      web3.eth.accounts.wallet.save(payload.pin, walletName)
      web3.eth.personal.unlockAccount(state.web3.admin, 'pass', 600)
        .then(() => {
          accountInstance.methods.addUser(
            web3.utils.toChecksumAddress(wallet[0]['address']),
            web3.utils.stringToHex(payload.email))
            .send({ from: state.web3.admin,
              gas: 4000000 })
            .then(async () => {
              try {
                // check first balance
                await web3.eth.getBalance(web3.utils.toChecksumAddress(wallet[0]['address']))

                // unlock admin account
                await web3.eth.personal.unlockAccount(state.web3.admin, 'pass', 600)

                // give ether from admin to account
                await web3.eth.sendTransaction({ from: state.web3.admin,
                  to: web3.utils.toChecksumAddress(wallet[0]['address']),
                  value: web3.utils.toWei('100', 'ether'),
                  gas: 8000000 })

                // check latest balance
                await web3.eth.getBalance(web3.utils.toChecksumAddress(wallet[0]['address']))
                // console.log('balance', balance)

                // authorize teacher
                await teacherInstance.methods.authorize(web3.utils.stringToHex(payload.email))
                  .send({
                    from: web3.utils.toChecksumAddress(wallet[0]['address']),
                    gas: 8000000
                  })
                console.log('teacher authorized')

                // add teacher
                const lecturer = payload.name
                const lecturerHex = web3.utils.stringToHex(lecturer)
                await teacherInstance.methods.addTeacher(lecturerHex)
                  .send({
                    from: web3.utils.toChecksumAddress(wallet[0]['address']),
                    gas: 8000000
                  })

                console.log('send done')
                resolve()
              } catch (e) {
                console.log('addTeacherAccount error', e)
                reject(e)
              }
            })
            .catch((err) => {
              console.log(err)
              reject(new Error('Unable to add account'))
            })
        }
        )
    })
  },
  addStudentAccount: ({
    commit,
    state
  }, payload) => { // payload = email, name, pin
    return new Promise(async (resolve, reject) => {
      const web3 = state.web3.instance()
      const accountInstance = state.web3.contract.accountManager
      const studentInstance = state.web3.contract.student
      const walletName = 'wallet_' + payload.email.split('@')[0]
      const wallet = web3.eth.accounts.wallet.create(1)
      localStorage.setItem('address', wallet[0]['address'])
      web3.eth.accounts.wallet.save(payload.pin, walletName)
      web3.eth.personal.unlockAccount(state.web3.admin, 'pass', 600)
        .then(() => {
          accountInstance.methods.addUser(
            web3.utils.toChecksumAddress(wallet[0]['address']),
            web3.utils.stringToHex(payload.email))
            .send({ from: state.web3.admin,
              gas: 8000000 })
            .then(async () => {
              try {
                // check first balance
                await web3.eth.getBalance(web3.utils.toChecksumAddress(wallet[0]['address']))

                // unlock admin account
                await web3.eth.personal.unlockAccount(state.web3.admin, 'pass', 600)

                // give ether from admin to account
                await web3.eth.sendTransaction({ from: state.web3.admin,
                  to: web3.utils.toChecksumAddress(wallet[0]['address']),
                  value: web3.utils.toWei('1', 'ether'),
                  gas: 8000000 })

                // check latest balance
                await web3.eth.getBalance(web3.utils.toChecksumAddress(wallet[0]['address']))
                // console.log('balance', balance)

                // authorize student
                await studentInstance.methods.authorize(web3.utils.stringToHex(payload.email))
                  .send({
                    from: web3.utils.toChecksumAddress(wallet[0]['address']),
                    gas: 8000000
                  })
                console.log('student authorized')

                resolve()
              } catch (e) {
                console.log('add student Account error', e)
                reject(e)
              }
            })
            .catch((err) => {
              console.log(err)
              reject(new Error('Unable to add account'))
            })
        }
        )
    })
  },
  checkAlreadyAccount: ({
    commit,
    state
  }, payload) => { // payload = email
    return new Promise((resolve, reject) => {
      const web3 = state.web3.instance()
      const accountInstance = state.web3.contract.accountManager
      accountInstance.methods.accounts(web3.utils.stringToHex(payload.email))
        .call((err, result) => {
          if (err) {
            console.log(err)
            reject(new Error('Unable to checkAlreadyAccount'))
          }
          commit('setAlreadyAccount', result)
          resolve(result)
        })
    })
  }
}
