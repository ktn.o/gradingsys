export default {
  web3: {
    admin: '',
    isInjected: false,
    instance: () => ({
      web3: {}
    }),
    contract: [
      {
        student: null,
        course: null,
        enrollment: null
      }
    ],
    accountManager: [],
    teacher: [],
    student: [
      // {
      //   code: '',
      //   name: '',
      //   grade: 0
      // }
    ],
    currentStudent: {},
    course: [
      // {
      //   name: '',
      //   lecturer: '',
      //   code: 0,
      //   section: 0,
      //   credit: 0,
      //   academic_year: 0
      // }
    ],
    enrollment: [
      // {
      //   student: null,
      //   course: null,
      //   grade: null
      // }
    ]
  },
  selectedCourse: '',
  userProfile: {
    // name: '',
    // email: ''
  },
  alreadyAccount: '',
  address: ''
}
