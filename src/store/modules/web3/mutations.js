export default {
  setAdmin (state, payload) {
    state.web3.admin = payload
  },
  setInjected (state, payload) {
    state.web3.isInjected = payload
  },
  setInstance (state, payload) {
    state.web3.instance = payload
  },
  setContract (state, payload) {
    state.web3.contract = payload
  },
  appendStudent (state, payload) {
    state.web3.student.push(payload)
  },
  appendCourse (state, payload) {
    state.web3.course.push(payload)
  },
  setStudent (state, payload) {
    state.web3.student = payload
  },
  setCourse (state, payload) {
    state.web3.course = payload
  },
  setSelectedCourse (state, payload) {
    state.selectedCourse = payload
  },
  clearCourse (state) {
    state.web3.course = []
  },
  clearStudent (state) {
    state.web3.student = []
  },
  setUserProfile (state, payload) {
    state.web3.userProfile = payload
  },
  setAlreadyAccount (state, payload) {
    state.alreadyAccount = payload
  },
  clearUserProfile (state) {
    state.userProfile = {}
  },
  clearAlreadyAccount (state) {
    state.alreadyAccount = ''
  },
  setAddress (state, payload) {
    state.address = payload
  }
}
