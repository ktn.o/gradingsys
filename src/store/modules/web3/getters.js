export default {
  admin: state => state.web3.admin,
  isInjected: state => state.web3.isInjected,
  instance: state => state.web3.instance(),
  contract: state => state.web3.contract,
  accountManager: state => state.web3.accountManager,
  teacher: state => state.web3.teacher,
  student: state => state.web3.student,
  currentStudent: state => state.web3.student,
  course: state => state.web3.course,
  enrollment: state => state.web3.enrollment,
  userProfile: state => state.web3.userProfile,
  alreadyAccount: state => state.web3.alreadyAccount,
  address: state => state.web3.address
  // transactionCount: state => {
  //   if (!state.web3.isInjected) {
  //     return 0
  //   }
  //   const web3 = state.web3.instance()
  //   const address = web3.utils.toChecksumAddress(localStorage.getItem('address'))
  //   return web3.eth.getTransactionCount(address, 'pending')
  //   // return web3.eth.getTransactionCount(address)
  // }
}
