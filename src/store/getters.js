// https://vuex.vuejs.org/en/getters.html

export default {
  isSignedIn (state) {
    return state.isSignedIn
  }
}
