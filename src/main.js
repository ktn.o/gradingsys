// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

// Components
import './components'

// Plugins
import './plugins'
import googleLogin from './plugins/googleLogin'

// Sync router with store
import { sync } from 'vuex-router-sync'

// Application imports
import App from './App'
import router from '@/router'
import { store } from '@/store'

// Sync store with router
sync(store, router)

Vue.config.productionTip = false
/* eslint-disable no-new */

const gauthOption = {
  clientId: '581754738437-jo5hgjlvohp3h38hmmflokano8f0isdj.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}
Vue.use(googleLogin, gauthOption)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
