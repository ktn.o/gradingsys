﻿## Academic Grading System

## Introduction
A student transcript management web application for KMITL students, teachers and officers. Implemented on go-ethereum blockchain using smart contracts to store the transcripts.

## Installation
yarn install

## Compiles and hot-reloads for development
yarn run serve

## Compiles and minfies for production
yarn run build

## Run the tests
yarn run test

## Lints and fixes files
yarn run lint